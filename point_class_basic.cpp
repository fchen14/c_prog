/* point_class.cpp: class example*/
#include <iostream>
using namespace std;
// use typedef real to switch double/float
typedef double real;

class Point { //define a class Point
private:      //list of private members
    int index; // index of the point
    char tag;  // name of the point
    real x;    // x coordinate
    real y;    // y coordinate
public:
    // use this function to set the private members
    void set_values(int,char,real,real);
    // use this function to output the private members
    void print_values();
};

// define the "set_values" method using scope operator "::"
void Point::set_values(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // overuse of comma operator :-)
}

// define the "print_values" method
void Point::print_values() {
   cout << "point " << tag << ": index = " << index 
        << ", x = " << x << ", y = " << y << endl;
}

int main(void) {
    Point p1, p2; // define two object of Point
    p1.set_values(0,'a',0,0); // object p1 use set_values method
    p2.set_values(1,'b',1,1); // object p2 use set_values method
    p1.print_values();        // object p1 use print_values method
    p2.print_values();        // object p2 use print_values method
    return 0;
}
