#include <stdio.h>
#include <string.h>
#define N 15

int main ()
{
    char str1[N] = "C program ";
    char str2[N] = "is great!";
    char str3[N];
    int  len;

    /* copy str1 into str3 */
    strcpy(str3, str1);
    printf("strcpy( str3, str1) :  %s\n", str3 );

    /* concatenates str1 and str2 */
    strcat( str1, str2);
    printf("strcat( str1, str2):   %s\n", str1 );

    /* total length of str1 after concatenation */
    len = strlen(str1);
    printf("strlen(str1) :  %d\n", len );

    char str4[] = "abcdefghi";
    char *str5 = "abcdefghi";
    printf("%d %d\n",strlen(str4),sizeof(str4)/sizeof(str4[0]));
    printf("%d %d\n",strlen(str5),sizeof(str5));    

    double arr[]={1,2,3,4,5,6,7,8,9,0};
    /* how to get the number of elements in an array*/
    /* http://c-faq.com/~scs/cclass/krnotes/sx9d.html */
    printf("%d\n",sizeof(arr)/sizeof(arr[0]));
    printf("%d\n",sizeof(arr)/sizeof(*arr));
    printf("%d\n",sizeof(arr)/sizeof*arr);

    return 0;
}
