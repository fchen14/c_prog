/* point_class.cpp: class example*/
#include <iostream>
using namespace std;
// use typedef real to switch double/float
typedef double real;

class Point { //define a class Point
private:      //list of private members
    int index; // index of the point
    char tag;  // name of the point
    real x;    // x coordinate
    real y;    // y coordinate
public:
    // declare a constructor to initialize members
    Point();
    // declare another constructor with parameters
    Point(int,char,real,real);
    // use this function to set the private members
    void set_values(int,char,real,real);
    // use another object to set the private members of the class
    void set_values(Point p);
    // use this function to output the private members
    void print_values();
    // overloading the + operator
    Point operator+(const Point& rhs);
    // overloading the << operator
    friend ostream& operator<< (ostream& stream, const Point& p);
};

// define a constructor to initialize members
// Note that no return type is used
Point::Point() {
    index=0, tag=0, x=0, y=0; //initialize the private members
}

// define another constructor with parameters
Point::Point(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // initialize with parameters
}

// define the "set_values" method using 4 values
void Point::set_values(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // overuse of comma operator :-)
}

// define the "set_values" method using another object 
void Point::set_values(Point p) {
    index=p.index, tag=p.tag, x=p.x, y=p.y;
}

Point Point::operator+(const Point& rhs){
    Point p;
    p.x = x + rhs.x;
    p.y = y + rhs.y;
    p.index = index;
    p.tag = tag;
    return p; 
}

ostream &operator<< (ostream& os, const Point& p){
    os << "point " << p.tag << ": index = " << p.index 
        << ", x = " << p.x << ", y = " << p.y << endl;
    return os;
}

// define the "print_values" method
void Point::print_values() {
   cout << "point " << tag << ": index = " << index 
        << ", x = " << x << ", y = " << y << endl;
}

int main(void) {
    Point p1, p2; // calling the Point() constructor
    p1.set_values(0,'a',0,0); // object p1 use set_values method
    p2.set_values(1,'b',1,1); // object p2 use set_values method
    p1.print_values();        // object p1 use print_values method
    p2.print_values();        // object p2 use print_values method
    Point p3(0,'c',0,1); // calling the Point(...) constructor
    Point p4(p3); // calling the Point(...) constructor
    p3.print_values();        // object p3 use print_values method
    p4.print_values();        // object p4 use print_values method
    Point p5; 
    p5.set_values(5,'e',0,0);
    p5 = p1 + p2;
    cout << p5;
    return 0;
}
