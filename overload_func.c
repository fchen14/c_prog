// overloading functions
#include <iostream>
using namespace std;
typedef double real;
// guarantee you get floating point result
//real divide(int a, int b) {
//  return ((real)a/b);
//}

real divide(real a, real b) {
  return (a/b);
}

int main () {
  int x=5,y=2;
  real n=5.0,m=2.0;
  cout << divide(x,y) << endl;
  cout << divide(n,m) << endl;
  return 0;
}
