#include <iostream>
using namespace std;

typedef struct Point {
    float x,y;
} point;

// T is a generic "Type"
template<typename T>
T add(T a, T b) {
    return a+b;
}

template<>
point add(point a, point b) {
    point p;
    p.x=a.x+b.x,p.y=a.y+b.y;
    return p;
}

int main() {
    int i=3;
    float f=5;
    point p1,p2;
    p1.x=2,p1.y=3;
    p2.x=4,p2.y=5;
//    cout << add(i,f) << endl;
    point p3=add(p1,p2);
    cout << p3.x << " " << p3.y << endl;
}

