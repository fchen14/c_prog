#include <stdio.h>
#define M 2 
#define N 2 
int main () {
   /* an array with 5 rows and 2 columns*/
   int a[2][2] = {{1,2}, {3,4}};
   int i, j;
 
   /* output each array element's value */
   for ( i = 0; i < M; i++ ) {
      for ( j = 0; j < N; j++ ) {
         printf("a[%d][%d] = %d ", i,j, a[i][j] );
      }
      printf("\n");
   }
   printf("a[0]=%p, &a[0][0]=%p\n",a[0], &a[0][0]);
   printf("a[1]=%p, &a[1][0]=%p\n",a[1], &a[1][0]);
   printf("now work on the array of address...\n");
   for ( i = 0; i < M; i++ ) {
      printf("a[%d] = %p\t&a[%d] = %p\n", i, a[i], i, &a[i] );
   } 
   printf("a=%p", a);
   return 0;
}
