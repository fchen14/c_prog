/* point_class.cpp: class example*/
#include <iostream>
#include <vector>
using namespace std;
// use typedef real to switch double/float
typedef double real;

class Point { //define a class Point
private:      //list of private members
    int index; // index of the point
    char tag;  // name of the point
    real x;    // x coordinate
    real y;    // y coordinate
public:
    // declare a constructor to initialize members
    Point();
    // declare another constructor with parameters
    Point(int,char,real,real);
    // use this function to set the private members
    void set_values(int,char,real,real);
    // use another object to set the private members of the class
    void set_values(Point p);
    // use this function to output the private members
    void print_values();
    // overloading the + operator
    Point operator+(const Point& rhs);
    // overloading the << operator
    friend ostream& operator<< (ostream& stream, const Point& p);
};

// define a constructor to initialize members
// Note that no return type is used
Point::Point() {
    index=0, tag=0, x=0, y=0; //initialize the private members
}

// define another constructor with parameters
Point::Point(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // initialize with parameters
}

// define the "set_values" method using 4 values
void Point::set_values(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // overuse of comma operator :-)
}

// define the "set_values" method using another object 
void Point::set_values(Point p) {
    index=p.index, tag=p.tag, x=p.x, y=p.y;
}

Point Point::operator+(const Point& rhs){
    Point p;
    p.x = x + rhs.x;
    p.y = y + rhs.y;
    p.index = index;
    p.tag = tag;
    return p; 
}

ostream &operator<< (ostream& os, const Point& p){
    os << "point " << p.tag << ": index = " << p.index 
        << ", x = " << p.x << ", y = " << p.y; 
    return os;
}

// define the "print_values" method
void Point::print_values() {
   cout << "point " << tag << ": index = " << index 
        << ", x = " << x << ", y = " << y << endl;
}

int main(void) {
    vector<Point> p_vec;
    // add 3 values to the p_vec object
    p_vec.push_back(Point(0,'a',0,3));         
    p_vec.push_back(Point(1,'b',1,2));        
    p_vec.push_back(Point(2,'c',2,1));       
    for(int i=0; i<p_vec.size(); i++) {
        // operator overloading
        cout << p_vec[i] << endl;
    }
    return 0;    
}
