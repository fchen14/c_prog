/* point_class.cpp: class example*/
#include <iostream>
#include <iomanip>
using namespace std;
// use typedef real to switch double/float
typedef double real;

class Point { //define a class Point
    protected:      //list of private members
        int index; // index of the point
        char tag;  // name of the point
        real x;    // x coordinate
        real y;    // y coordinate
    public:
        // declare a constructor to initialize members
        Point();
        // declare another constructor with parameters
        Point(int,char,real,real);
        // use this function to set the private members
        void set_values(int,char,real,real);
        // use another object to set the private members of the class
        void set_values(Point p);
        // use this function to output the private members
        void print_values();
};

// define a constructor to initialize members
// Note that no return type is used
Point::Point() {
    index=0, tag=' ', x=0, y=0; //initialize the private members
}

// define another constructor with parameters
Point::Point(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // initialize with parameters
}

// define the "set_values" method using 4 values
void Point::set_values(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // overuse of comma operator :-)
}

// define the "set_values" method using another object 
void Point::set_values(Point p) {
    index=p.index, tag=p.tag, x=p.x, y=p.y;
}

// define the "print_values" method
void Point::print_values() {
    cout << "point " << tag << ": index = " << index 
        << ", x = " << x << ", y = " << y << endl;
}

// declare a derived class Particle based on Point
class Particle: public Point {
    protected:
        real mass;
    public:
        Particle(){ mass=0.0; };
        void set_mass(real);
        real get_mass();
};
// define the set_mass method
void Particle::set_mass(real m){
    mass = m;
}
// define the get_mass method
real Particle::get_mass(){
    return mass;
}

int main(void) {
    Particle p; // which constructor is called?
    // calls the base class method (function)
    p.set_values(1,'a',0.5,1.0);
    p.print_values();
    // calls the derived class method (function)
    p.set_mass(1.3);        
    // read how to control the format using cout
    // http://www.cplusplus.com/reference/ios/ios_base/precision/
    cout << "mass of p = " << fixed << setprecision(3) 
         << p.get_mass() << endl;  
    return 0;
}
