/* point_class.cpp: class example*/
#include <iostream>
#include <stdlib.h>
using namespace std;
// use typedef real to switch double/float
typedef double real;

class Point { //define a class Point
private:      //list of private members
    int index; // index of the point
    char tag;  // name of the point
    real x;    // x coordinate
    real y;    // y coordinate
public:
    // declare a constructor to initialize members
    Point();
    // declare another constructor with parameters
    Point(int,char,real,real);
    // declare a destructor
    ~Point();
    // use this function to set the private members
    void set_values(int,char,real,real);
    // use this function to output the private members
    void print_values();
};

// define a constructor to initialize members
// Note that no return type is used
Point::Point() {
    cout << "Constructor called." << endl;
    index=0, tag=0, x=0, y=0; //initialize the private members
}

// define another constructor with parameters
Point::Point(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // initialize with parameters
}

// define the destructor
Point::~Point() {
    cout << "Destructor called." << endl;
}

// define the "set_values" method using scope operator "::"
void Point::set_values(int idx,char tg,real xc,real yc) {
    index=idx, tag=tg, x=xc, y=yc; // overuse of comma operator :-)
}

// define the "print_values" method
void Point::print_values() {
   cout << "point " << tag << ": index = " << index 
        << ", x = " << x << ", y = " << y << endl;
}

int main(void) {
    Point *ptr_p = new Point[2];
    delete[] ptr_p;
    cout << "using malloc:" << endl;
    ptr_p =(Point*)malloc(2*sizeof(Point));
    free(ptr_p);
    return 0;
}
