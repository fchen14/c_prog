#include <iostream>
// use standard libraries
using namespace std;
// we are using C++ style comments
int main () {   
    int n = 2*3; // Simple declaration of i
    int *a = new int[n]; //use "new" to manage storage
    // C++ style output
    cout << "Hello world with C++" << endl; 
    for (int i = 0; i < n ; i++) {  // Local declaration of i 
        a[i]=i;
        // we are using C++ cout for output
        cout << "a[" << i << "] = " << a[i] << endl; 
    }
    delete[] a; // free the memory space we used
    return 0;
}
