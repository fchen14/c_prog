/* pointer_null.c */
#include <stdio.h>

int main () {
    /* set the pointer to NULL "0" */
    int  *ptr = NULL;
    /* succeeds if p is null */
    if(ptr) printf("The value of the pointer is %p\n", ptr);
    /* succeeds if p is not null */
    if(!ptr) printf("The value of the pointer is %p\n", ptr);
    int *p;
    printf("The value of the pointer is %p\n", p);
    *p=3;
    printf("The value of the pointer is %d\n", *p);
    return 0;
}
