/* dynamic_2d_array.c */
#include <stdio.h>
#include <stdlib.h>
#define M 2
#define N 2
int main(void) {
    int i,nrows=M,ncols=N,array_static[M][N];

    printf("\naddress of static array_dynamic\n");
    for (i=0; i<nrows; i++){
        printf("array_static[%d]=%p\n",i,array_static[i]);
    }

    for (i=1; i<nrows; i++){
        printf("array_static[%d]-array_static[%d]=%d\n",i,i-1,array_static[i] - array_static[i-1]);
    }

    /* Define the size of the array_dynamic at run time */
    /* printf("Enter number of rows and number of columns \n");
       scanf("%d %d",&nrows,&ncols);*/

    int** array_dynamic;
    array_dynamic=(int**)malloc(nrows*sizeof(int*));
    printf("\naddress of initial dynamic malloc method:\n");
    for (i=0; i<nrows; i++){
        array_dynamic[i]=(int*)malloc(ncols*sizeof(int));
        printf("array_dynamic[%d]=%p\n",i,array_dynamic[i]);
    }

    for (i=1; i<nrows; i++){
        printf("array_dynamic[%d]-array_dynamic[%d]=%d\n",i,i-1,array_dynamic[i] - array_dynamic[i-1]);
    }

    for (i=0; i<nrows; i++)
        free((void*)array_dynamic[i]);
    free((void*)array_dynamic);

    /* here is the method to correct the non-contiguous memory problem */
    array_dynamic=(int**)malloc(nrows*sizeof(int*));
    array_dynamic[0]=(int*)malloc(nrows*ncols*sizeof(int));
    for (i=1; i<nrows; i++){
        array_dynamic[i]=array_dynamic[0]+ncols*i;
    }

    printf("\naddress of improved dynamic malloc method:\n");
    for (i=0; i<nrows; i++){
        printf("array_dynamic[%d]=%p\n",i,array_dynamic[i]);
    }

    for (i=1; i<nrows; i++){
        printf("array_dynamic[%d]-array_dynamic[%d]=%d\n",i,i-1,array_dynamic[i] - array_dynamic[i-1]);
    }

    free((void*)array_dynamic[0]);
    free((void*)array_dynamic);
    return 0;
}
