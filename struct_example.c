/* struct_example.c */
#include <stdio.h>

typedef double real; 
/* typedef float real; easy switch between precisions*/

typedef struct Point { 
    int index;
    char tag;
    real x;
    real y;
} Point;

void print_point(struct Point point);

int main() {
    /* define two struct Point variables */
    /* struct Point p1, p2; */
    Point p1, p2, p3; 
    Point *ptr_p; /* define a pointer to Point */
    ptr_p = &p3;  /* point prt_p to struct p3  */
    /* assign values to struct members of p1 */
    p1.index=0;
    p1.tag = 'a';
    p1.x = 0.0;
    p1.y = 0.0;
    /* assign values to struct members of p2 */
    p2.index=1, p2.tag = 'b', p2.x = 1.0, p2.y = 1.0; 
    p3 = p1; /* assign struct var p1 to var p3 */
    /* output p1 and p2 */
    print_point(p1);
    print_point(p2);
    print_point(p3);
    /* access the struct member through pointer */
    printf("tag=%c\n", ptr_p->tag); 
    /* access the struct member through dereference operator */
    printf("tag=%c\n", (*ptr_p).tag);
}

void print_point(struct Point point)
{
    printf( "\npoint %c:\n", point.tag);
    printf( "\tindex : %d\n", point.index);
    printf( "\tx = %7.2lf\n", point.x);
    printf( "\ty = %7.2lf\n", point.y);
    printf( "\n" );
}
