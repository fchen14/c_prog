#include <iostream>
using namespace std;
// example using C++ reference
int main() {
    int x,y=4;
    int& rx = x;// declare a reference for x
    rx = 3;// rx is now a reference to x so this sets x to 3
    cout << "before: x=" << x << " y=" << y << endl;
    swap(x,y);
    cout << "after : x=" << x << " y=" << y << endl;
}
void swap (int& a, int& b) {// without the need for pointers
    int t;
    t=a,a=b,b=t;
}
