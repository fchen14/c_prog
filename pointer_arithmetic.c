/* pointer_array.c */
#include <stdio.h>

int main() {
  int *p;
  int a[10] = {1,2,3,4,5,6,7,8,9,10};

  p = a; 
  p = &a[0]; /* equilvalent statement */

  printf("The pointer is pointing to the first ");
  printf("array element, which is %d.\n", *p);
  printf("Let's increment it.....\n");

  p++;

  printf("Now it should point to the next element,");
  printf(" which is %d.\n", *p);
  printf("But suppose we point to the 3rd and 4th: %d %d.\n", 
          *(p+1),*(p+2));

  p+=2;

  printf("Now skip the next 4 to point to the 8th: %d.\n", 
          *(p+=4));

  p--;

  printf("Did I miss out my lucky number %d?!\n", *(p++));
  printf("Back to the 8th it is then..... %d.\n", p[0]);

  return 0;
}
