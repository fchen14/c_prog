#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;
// using STL to: (1) find a value in an array (2) sort the array
int main() {
    int arr[]={2,3,1,5,4,6,0,7,9,8};
    int *p = find(arr,arr+10,5); // find number "5" in the array using std::find
    p++;
    cout << "The number after 5 is " << *p << endl;
    vector<int> vec (arr,arr+10); // assign the array values to std::vector
    vec.push_back(11);
    vec.push_back(-1);
    cout << "Now the length of the vector is:" << vec.size() << endl;
    // now sort the array
    sort(vec.begin(),vec.end());
    for(int i=0; i<vec.size(); i++)
        cout << vec[i]<< " ";
    cout << endl;
    return 0;
}
