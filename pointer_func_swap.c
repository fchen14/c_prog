#include <stdio.h>

/* function declaration.*/
void swap( int p1, int p2 );
void swap_by_reference( int *p1, int *p2 );

/* this is the main calling function */
int main() {
    int a = 2;
    int b = 3;
    printf("swap by value:\n");
    printf("Before:  a = %d and  b = %d\n", a, b );
    /* call by value */
    swap( a, b );
    printf("After:  a = %d and  b = %d\n", a, b );
    printf("swap by reference:\n");
    printf("Before:  a = %d and  b = %d\n", a, b );
    /* call by reference */
    swap_by_reference( &a, &b );
    printf("After:  a = %d and  b = %d\n", a, b );
}

/* pass by value */
void swap(int p1, int p2) {
    int t;
    t = p2, p2 = p1, p1 = t;
    printf("Swap: a (p1) = %d and  b(p2) = %d\n", p1, p2 );
}

/* pass by reference */
void swap_by_reference(int *p1, int *p2) {
    int t;
    t = *p2, *p2 = *p1, *p1 = t;
    printf("Swap: a (p1) = %d and  b(p2) = %d\n", *p1, *p2 );
}
